# jlame

jlame library

Original port: by http://dmilvdv.narod.ru/Apps/oggvorbis.html

Original code: https://www.xiph.org/vorbis/

## Cetral Maven Repo

```xml
<dependency>
  <groupId>com.github.axet</groupId>
  <artifactId>jvorbis</artifactId>
  <version>1.3.6</version>
</dependency>
```

## Android Studio

```gradle
    api 'com.github.axet:jvorbis:1.3.6'
```
